public class Random {

    public int getRandomNumberBetweenMinAndMax(int min, int max){
        return (int) (Math.random() * (max - min + 1)) + min;
    }

    public void printRandomNumberBetweenMinAndMaxNTimes(int min, int max, int nTimes){
        for(int i = 0; i < nTimes; i++){
            System.out.println(getRandomNumberBetweenMinAndMax(min, max));
        }
    }
}
