public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        random.printRandomNumberBetweenMinAndMaxNTimes(0, 1, 1);
        System.out.println("************************");
        random.printRandomNumberBetweenMinAndMaxNTimes(0, 1, 10);
        System.out.println("************************");
//        Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
        random.printRandomNumberBetweenMinAndMaxNTimes(0, 10, 10);
        System.out.println("************************");
//        Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.
        random.printRandomNumberBetweenMinAndMaxNTimes(20, 50, 10);
        System.out.println("************************");
//        Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
        random.printRandomNumberBetweenMinAndMaxNTimes(-10, 10, 10);
        System.out.println("************************");
//        Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел, каждое в диапазоне от -10 до 35.
        random.printRandomNumberBetweenMinAndMaxNTimes(-10, 35, random.getRandomNumberBetweenMinAndMax(3,15));
        System.out.println("************************");

    }
}
