import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomTest {

    Random random = new Random();

    @Test
    void getRandomNumberBetweenMinAndMaxTest1() {
        boolean actual = true;
        int min = -10;
        int max = 10;
        int result = 0;
        for (long i = 0l; i < 1_000_000l; i++) {
            result = random.getRandomNumberBetweenMinAndMax(min,max);
            if(result < min || result > max){
                actual = false;
            }
        }
        assertTrue(actual);
    }
}